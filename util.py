from skaero.atmosphere import coesa
from math import pi
import operator

operators = {
    "<": operator.lt,
    ">": operator.gt,
    "==": operator.eq,
}


def export_results(filename, results, pad=15):
    header = "\t".join([f"{c:>{pad}}" for c in results.columns])
    with open(filename, "w") as file:
        file.write(header + "\n")
        for row in results.iterrows():
            row = row[1]
            line = "\t".join([f"{elem:{pad}.3f}" for elem in row])
            file.write(line + "\n")


def density(altitude):
    altitude *= 0.3048  # convert to meters
    _, _, _, rho = coesa.table(altitude)
    return rho * 0.062428


def runge_kutta(func, u, p, dt):
    """
    Modified Runge-Kutta function that returns the state derivative in addition
    to the modified state vector.
    """
    k1 = dt * func(dt, u, p)
    k2 = dt * func(dt / 2, u + k1 / 2, p)
    k3 = dt * func(dt / 2, u + k2 / 2, p)
    k4 = dt * func(dt, u + k3, p)
    du = (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return u + du, du / dt


class Simulation:
    """
    Parameters
    -------------------------------------------------------------------------
    eom_func - user defined function that describes the equation of motion
    u        - initial state vector
    p        - initial control vector
    dt       - simulation time step
    end_time - when to stop simulation
    """

    def __init__(self, eom_func, u, p, dt, end_time):
        self.t = 0
        self.eom_func = eom_func
        self.u = u
        self.p = p
        self.dt = dt
        self.end_time = end_time

        _, du = runge_kutta(self.eom_func, self.u, self.p, self.dt)
        self.u_vecs = [tuple(u)]
        self.du_vecs = [tuple(du)]
        self.time = [self.t]

    def run(self, state, op, condition):
        """
        Parameters
        -------------------------------------------------------------------------
        state     - the index of the state vector which defines the stop condition
        op        - the comparison operator, passed as a string (ex: "<" "==")
        condition - the stop condition for the simulation 

        Returns
        -------------------------------------------------------------------------
        time    - time vector
        u_vecs  - an array of tuples, which contains the state at each time step
        du_vecs - like u_vecs, but with the state derivative at each step 
        """
        conditional = operators[op]
        while conditional(self.u[state], condition):
            self.u, du = runge_kutta(self.eom_func, self.u, self.p, self.dt)
            self.time.append(self.t)
            self.u_vecs.append(tuple(self.u))
            self.du_vecs.append(tuple(du))
            self.t += self.dt
        return self.time, self.u_vecs, self.du_vecs

