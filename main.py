import numpy as np
import random
import matplotlib.pyplot as plt
import pandas as pd
from util import *
from math import log

NEWTTOLB = 0.224809

L2200G = pd.read_csv("L2200G", delim_whitespace=True, header=None)
DRAG_COEFF = pd.read_csv("drag_coeff.csv", delimiter="\t", header=None)

# time, mass, altitude, velocity
u = np.array([0, 57.9, 0, 0, 0])
p = np.zeros(2)


def mass_flow_rate(thrust, total_imp, prop_weight):
    spec_imp = total_imp / prop_weight  # total impulse divided by propellant weight
    return -thrust / (spec_imp * 32.174)


def EOM(dt, u, p):
    """
    du[0] = change in time
    du[1] = change in mass
    du[2] = velocity
    du[3] = acceleration
    """
    du = np.zeros(len(u))
    tot_imp = 1147.42
    prop_weight = 5.57

    A = pi * ((6.4 / 12) ** 2) / 4
    g = 32.174
    Cd = 0.585  # np.interp(u[0], DRAG_COEFF[0], DRAG_COEFF[1])
    thrust = np.interp(u[0], L2200G[0], L2200G[1]) * NEWTTOLB

    du[0] = 1  # time, multiplied by dt in the runge kutta method
    du[1] = mass_flow_rate(thrust, tot_imp, prop_weight) * g  # multiply by g to get lbm
    du[2] = u[3]  # velocity
    du[3] = (
        -g
        - (Cd * density(u[2]) * A * u[3] ** 2) / (2 * u[1])
        + (thrust / (u[1] / g))
        + np.random.normal(0, 1)
    )  # acceleration
    u[4] = du[3]
    return du


sim = Simulation(EOM, u, p, 0.05, 15)
time, u_vecs, du_vecs = sim.run(3, ">", -1)
time, mass, alts, velocs, T = zip(*u_vecs)
_, _, _, accel, _ = zip(*du_vecs)

results = pd.DataFrame(
    {
        "Time": time,
        "Mass": mass,
        "Altitude": alts,
        "Velocity": velocs,
        "Acceleration": accel,
        "P_alt": T,
    }
)

# results.to_csv("tets", index=False, float_format="%10.3f", sep="\t")
export_results("tets2", results)

print(results.Altitude.max())

results.plot(x="Time", y="P_alt")
# results.plot(x="Time", y=["Acceleration"])
plt.show()
